export interface IAuthData {
  email: string;
  password: string;
}

export interface ISignupData {
  name: string;
  email: string;
  password: string;
  companyName: string;
}

export const signupInitialStateData: ISignupData = {
  name: '',
  email: '',
  password: '',
  companyName: '',
};

export interface IUserData {
  name: string;
  email: string;
  password: string;
  companyName: string;
}

export const userInitialStateData: IUserData = {
  name: '',
  email: '',
  password: '',
  companyName: '',
};
