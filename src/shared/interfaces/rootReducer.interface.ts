import {
  IAuthData,
  ISignupData,
  IUserData,
} from '../interfaces/user.interface';
import { UserActionTypes } from '../types/user.types';

export interface IApplicationState {
  user: IUserData;
  login: IAuthData;
  signup: ISignupData;
}

/**
 * General action
 */
export interface IAction<IApplicationState> {
  type: UserActionTypes;
  payload?: IApplicationState;
}
