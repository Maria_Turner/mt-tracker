export enum UserActionTypes {
  GET_USER_DATA = 'user/GET_USER_DATA',
  SAVE_USER_DATA = 'user/SAVE_USER_DATA',
}

export enum AuthActionTypes {
  LOGGED_IN = 'user/LOGGED_IN',
  LOGGED_OUT = 'user/LOGGED_OUT',
  SIGN_UP = 'user/SIGN_UP',
}
