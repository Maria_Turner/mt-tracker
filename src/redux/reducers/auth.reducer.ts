import { Reducer } from 'redux';

import {
  IAuthData,
  authInitialStateData,
} from '../../shared/interfaces/user.interface';
import { AuthActionTypes } from '../../shared/types/user.types';

export const authReducer: Reducer<IAuthData> = (
  state = authInitialStateData,
  { type, ...payload }
) => {
  switch (type) {
    case AuthActionTypes.LOGGED_IN:
      return {
        ...state,
        ...payload,
      };
    case AuthActionTypes.LOG_OUT:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default authReducer;
