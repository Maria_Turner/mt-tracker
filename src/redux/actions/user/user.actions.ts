import axiosInstance from '../../../config/axiosConfig';
import {
  IAuthData,
  IUserData,
  ISignupData,
} from '../../../shared/interfaces/user.interface';
import {
  AuthActionTypes,
  UserActionTypes,
} from '../../../shared/types/user.types';
import { AppDispatch } from '../../store';

export const getData =
  () =>
  async (dispatch: AppDispatch): Promise<IUserData> => {
    try {
      const response = await axiosInstance.get('/users/list');
      if (response.status === 200) {
        dispatch({
          type: UserActionTypes.GET_USER_DATA,
          payload: response.data.data,
        });
      }
      return response.data;
    } catch (error) {
      return error;
    }
  };

export const saveData =
  (body: IUserData) =>
  async (dispatch: AppDispatch): Promise<IUserData> => {
    try {
      const response = await axiosInstance.post('/signUp', body);
      if (response.status === 200) {
        dispatch({
          type: UserActionTypes.SAVE_USER_DATA,
        });
      }
      return response.data.data;
    } catch (error) {
      return error;
    }
  };

export const authData =
  (body: IAuthData) =>
  async (dispatch: AppDispatch): Promise<IAuthData> => {
    try {
      const response = await axiosInstance.post('/login', body);
      console.log('auth', response);
      if (response.status === 200 && !response.data.hasError) {
        axiosInstance.defaults.headers.common.Authorization = `Bearer ${response.data.data.token}`;
        dispatch({
          type: AuthActionTypes.LOGGED_IN,
        });
      }
      console.log('jjj', response.data);
      return response.data;
    } catch (error) {
      console.log('jjj', error);
      return error;
    }
  };

export const userSignOut =
  (body: IAuthData) =>
  async (dispatch: AppDispatch): Promise<IAuthData> => {
    try {
      axiosInstance.defaults.headers.common.Authorization = null;
      dispatch({
        type: AuthActionTypes.LOGGED_OUT,
      });
    } catch (error) {
      return error;
    }
  };
export const userSignUp =
  (body: ISignupData) =>
  async (dispatch: AppDispatch): Promise<IAuthData> => {
    try {
      // const formData = await jsonToFormData(body);
      const response = await axiosInstance.post('/signUp', body);
      dispatch({
        type: AuthActionTypes.SIGN_UP,
      });
      return response.data;
    } catch (error) {
      return error;
    }
  };
// export const loginUser =
//     (body: IUserData) =>
//     async (dispatch: AppDispatch): Promise<IUserData> =>
// axiosInstance.post('/logIn', body).then((res) => {
//  const token = `Bearer ${res.data.token}`;
//  localStorage.setItem(‘token’, `Bearer ${res.data.token}`);//setting token to local storage
//  axiosInstance.defaults.headers.common[‘Authorization’] = token;//setting authorize token to header in axios
//  dispatch(getUserData());
//  dispatch({ type: CLEAR_ERRORS });
//  console.log(‘success’);
//  history.push(‘/’);//redirecting to index page after login success
// })
//  .catch((err) => {
//  console.log(err);
//  dispatch({
//  type: SET_ERRORS,
//  payload: err.response.data
// });
// });
// }
