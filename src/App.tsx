import React from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { routePaths } from './config/globalConfig';
import Login from './pages/Login';
import Register from './pages/Register';
// eslint-disable-next-line import/no-unresolved
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css';
// const Login = lazy(() => import('./pages/Login'))
// const Register = lazy(() => import('./pages/Register'));

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function App() {
  return (
    // <Router>
    //   <h1>rgvre</h1>
    //   <Switch>
    //     <Route path={routePaths.root} component={Register} />
    //   </Switch>
    // </Router>
    <Router>
      <Switch>
        <Route path={routePaths.register} component={Register} />
        <Route path={routePaths.root} component={Login} />
      </Switch>
    </Router>
  );
}
function Home() {
  return <h2>Home</h2>;
}
