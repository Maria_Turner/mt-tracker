import React, { useEffect, useState } from 'react';

import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

import { routePaths } from '../../config/globalConfig';
// import Images from '../../constants/images';
import { authData } from '../../redux/actions/user/user.actions';
import { AppDispatch } from '../../redux/store';
import {
  IAuthData,
  authInitialStateData,
} from '../../shared/interfaces/user.interface';
import Image from '../../public/Images/demo.jpg';
import Logo from '../../public/Images/logo.jpg';

const Login: React.FC = () => {
  const dispatch: AppDispatch = useDispatch();
  const [emails, setEmail] = useState('');
  const [passwords, setPassword] = useState('');
  const history = useHistory();
  const handleClick = () => {
  
    const body = {
      email: emails,
      password: passwords,
    };
    console.log(body, 'response');
    dispatch(authData(body)).then(() => {
      // console.log(res, 'response');
      // if (!res.hasError) {
      history.push({
        pathname: routePaths.register,
        });
        // } else {
          // history.push({
          //   pathname: routePaths.root,
          // });
      });
    };
  };
  return (
    <div className="login">
      <Container>
        <Row>
          <Col xs="5">
            <img src={Logo} alt="logo"></img>
            <p>Login your account using email & passworrd.</p>

            <Form>
              <FormGroup>
                <Input
                  type="email"
                  name="x`"
                  id="exampleEmail"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  id="examplePassword"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormGroup>
              <Button type="primary" onClick={() => handleClick()}>
                Login
              </Button>{' '}
            </Form>
            <Row>
              <Col xs="6">
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" /> Remember Me
                  </Label>
                </FormGroup>
              </Col>
              <Col xs="6">
                <FormGroup>
                  <a href="">Forget Password?</a>
                </FormGroup>
              </Col>
            </Row>

            <div onClick={() => history.push(routePaths.register)}>
              Don't have an account yet? Create an account
            </div>
          </Col>
          <Col xs="7">
            <img src={Image} />
            {/* <img src={Images.login}></img> */}
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
