import React, { useEffect, useState } from 'react';

import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Row,
} from 'reactstrap';

import { routePaths } from '../../config/globalConfig';
import { userSignUp } from '../../redux/actions/user/user.actions';
import { AppDispatch } from '../../redux/store';
import {
  IAuthData,
  userInitialStateData,
} from '../../shared/interfaces/user.interface';
// import Images from '../../constants/images';
import Image from '../../public/Images/demo.jpg';
import Logo from '../../public/Images/logo.jpg';

const Register: React.FC = () => {
  const dispatch: AppDispatch = useDispatch();
  const [emails, setEmail] = useState('');
  const [passwords, setPassword] = useState('');
  const [companies, setCompany] = useState('');
  const history = useHistory();

  const handleClick = () => {
    const body = {
      name: '',
      email: emails,
      password: passwords,
      companyName: companies,
    };
    // console.log(body, 'response');
    dispatch(userSignUp(body)).then(
      (res: IAuthData) => {
        console.log(res, 'response');
        if (!res.hasError) {
          history.push({
            pathname: routePaths.root,
          });
        } else {
          history.push({
            pathname: routePaths.register,
          });
        }
      },
      [dispatch]
    );
  };
  return (
    <div className="register">
      <Container>
        <Row>
          <Col xs="5">
            <img src={Logo} alt="logo"></img>
            <p>Create your account using email.</p>
            <Form>
              <FormGroup>
                <Input
                  type="companyname"
                  name="companyname"
                  id="companyname"
                  placeholder="Company name"
                  onChange={(e) => setCompany(e.target.value)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="email"
                  name="email"
                  id="exampleEmail"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  id="examplePassword"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormGroup>
              <Button color="primary" onClick={() => handleClick()}>
                Register
              </Button>{' '}
            </Form>
            <div>
              Already have an account?
              <a onClick={() => history.push(routePaths.root)}> Login</a>
            </div>
          </Col>
          <Col xs="7">
            <img src={Image} />
            {/* <img src={Images.login}></img> */}
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Register;
